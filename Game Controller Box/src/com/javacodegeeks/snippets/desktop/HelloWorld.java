package com.javacodegeeks.snippets.desktop;

import com.box.controllers.components.commlayers.Hook;
import com.box.controllers.components.model.MessageEvent;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;

import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

public class HelloWorld implements NativeKeyListener, NativeMouseListener, NativeMouseMotionListener {
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */


    static {
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(false);
    }

    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        JFrame frame = new JFrame("HelloWorldSwing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add the ubiquitous "Hello World" label.
        JLabel label = new JLabel("Hello World");
        frame.getContentPane().add(label);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        HelloWorld helloWorld = new HelloWorld();
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.exit(1);
        }
        GlobalScreen.addNativeKeyListener(helloWorld);
        GlobalScreen.addNativeMouseListener(helloWorld);
        GlobalScreen.addNativeMouseMotionListener(helloWorld);

        /*javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });*/
    }


    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {

        String keyName = "not specified";
        keyName = NativeKeyEvent.getKeyText(e.getKeyCode());
        System.out.println("Key Pressed: " + keyName);

        if (e.getKeyCode() == NativeKeyEvent.ALT_R_MASK) {

        } else {
            Hook.getInstance().dispatch(new MessageEvent(e.getKeyCode(), keyName));
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        System.out.println("in key released");
        // Hook.getInstance().dispatch(new Event(e.getKeyCode(), NativeKeyEvent.getKeyText(e.getKeyCode())));

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        System.out.println("in key typed");
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {

        System.out.println("mouse clicked");

    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseMoved(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {

    }
}