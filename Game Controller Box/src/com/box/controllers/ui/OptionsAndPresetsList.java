package com.box.controllers.ui;

import com.box.controllers.components.model.Option;
import com.box.controllers.components.model.Preset;
import com.box.controllers.components.model.Presets;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OptionsAndPresetsList {
    // the flat list of options and presets
    private final ListProperty<NamedInstance> optionsAndPresets = new SimpleListProperty<NamedInstance>(FXCollections.observableArrayList());
    // a ChangeListener for changes in a list of presets
    private final ChangeListener<ObservableList<Preset>> presetChangeListener = new ChangeListener<ObservableList<Preset>>() {
        @Override
        public void changed(ObservableValue<? extends ObservableList<Preset>> observable,
                            ObservableList<Preset> oldValue, ObservableList<Preset> newValue) {
            refreshList();
        }

    };

    public OptionsAndPresetsList(ListProperty<Option> optionsProperty) {
        // add a ChangeListener for changes in the list of Options
        optionsProperty.addListener(new ChangeListener<ObservableList<Option>>() {
            @Override
            public void changed(
                    ObservableValue<? extends ObservableList<Option>> observable,
                    ObservableList<Option> oldValue,
                    ObservableList<Option> newValue) {
                // make sure to have listeners on each preset, and to remove the listeners
                // again when options are removed
                if (oldValue != null) {
                    for (Option option : oldValue) {
                        option.presetsProperty().removeListener(presetChangeListener);
                    }
                }
                if (newValue != null) {
                    for (Option option : newValue) {
                        option.presetsProperty().addListener(presetChangeListener);
                    }
                }
                refreshList();
            }
        });
    }

    private void refreshList() {
        // clear the list and re-add all Options and Presets.
        // this is not effective, but more readable than keeping track
        // a each change in the list
        optionsAndPresets.clear();
        for (Option option : Presets.optionsProperty().get()) {
            optionsAndPresets.add(option);
            for (Preset preset : option.presetsProperty().get()) {
                optionsAndPresets.add(preset);
            }
        }
    }

    public ListProperty<NamedInstance> optionsAndPresetsProperty() {
        return optionsAndPresets;
    }
}