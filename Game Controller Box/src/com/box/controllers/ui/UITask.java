package com.box.controllers.ui;

import com.box.controllers.components.commlayers.AppUnit;
import com.box.controllers.components.commlayers.Hook;
import com.box.controllers.components.eventbinder.EventBinder;
import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.Profile;
import com.box.controllers.components.unit.recorder.Recorder;
import com.box.controllers.components.unit.windows.WindowRecorder;
import com.box.controllers.exception.IllegalTaskCreationException;
import com.box.controllers.utils.ThreadManager;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.util.*;


/**
 * the UI task is created and executed only under a UI Threads per request for bindings and other activities
 * on a particular profile...
 */
public class UITask {

    private static final String BOX_BUTTON = "Box Button";
    private static final String MAKE_BUTTON = "Make Button";
    private static BoxButtonUI boxButtonUI = null;
    private static ProfilesPalletUI profilesPallet = null;
    private static CheckBoxUI checkBoxUI = null;
    private static ObservableList<Profile> profiles = null;
    private static EventHandler<ActionEvent> boxButtonHandler1;
    private static EventHandler<ActionEvent> boxButtonHandler2;
    private static Executor process = null;
    private static String mode = "F"; //indicates foreground mode (possible values 'F' and 'B' and 'NA') and events captured on app's windows..
    private static boolean minimized = false;
    private static OptionsAndPresetsList optionsAndPresetsList = null;
    private ProfileUI profileUI;
    private List<TextField> textFieldList;

    /* initializing box buttons handlers */ {
        if (boxButtonHandler1 == null) {
            boxButtonHandler1 = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        ThreadManager.getInstance().createUITask(process, event);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            };
        }

        if (boxButtonHandler2 == null) {
            boxButtonHandler2 = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        ThreadManager.getInstance().createUITask(process, event);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            };
        }
    }

    public UITask() {

    }

    public UITask(Profile profile) {
        textFieldList = new ArrayList<>();
        if (!Platform.isFxApplicationThread()) {
            System.out.println("running job in Background Thread..." + profile.toString());
            UITask.profiles.add(profile);
            profileUI = new ProfileUI();
            profileUI.setProfile(profile);
            if (profile.isDefault())
                Container.setCurrentTask(this); //setting default UI...
        } else {
            System.out.println("running job in UI Thread..." + profile.toString());
            profile.setDefault(true);
            UITask.profiles.add(profile);
            profileUI = new ProfileUI();
            profileUI.setProfile(profile);
            Container.setCurrentTask(this); //setting default UI...
        }
    }

    public static OptionsAndPresetsList getOptionsAndPresetsList() {
        return optionsAndPresetsList;
    }

    public static void setOptionsAndPresetsList(OptionsAndPresetsList optionsAndPresetsList) {
        UITask.optionsAndPresetsList = optionsAndPresetsList;
    }

    public static CheckBoxUI getCheckBoxUI() {
        return checkBoxUI;
    }

    public static void setCheckBoxUI(CheckBoxUI checkBoxUI) {
        UITask.checkBoxUI = checkBoxUI;
    }

    public static boolean isMouseMoveEnabled() {
        return checkBoxUI.getMouseMoveRecording().isSelected();
    }

    public static boolean isMouseClickEnabled() {
        return checkBoxUI.getMouseClickRecording().isSelected();
    }

    public static boolean isKeyPressedEnabled() {
        return checkBoxUI.getKeyPressRecording().isSelected();
    }

    public static boolean isBackgroundEnabled() {
        return checkBoxUI.getBackgroundRecording().isSelected();
    }

    public static void setMouseMoveEnabled() {
        checkBoxUI.getMouseMoveRecording().setSelected(true);
    }

    public static void setMouseClickEnabled() {
        checkBoxUI.getMouseClickRecording().setSelected(true);
    }

    public static void setKeyPressedEnabled() {
        checkBoxUI.getKeyPressRecording().setSelected(true);
    }

    public static void setBackgroundEnabled() {
        checkBoxUI.getBackgroundRecording().setSelected(true);
    }

    public static boolean isMinimized() {
        return minimized;
    }

    public static void setMinimized(boolean minimized) {
        UITask.minimized = minimized;
    }

    public static String getMode() {
        return mode;
    }

    public static void setMode(String mode) {
        UITask.mode = mode;
    }

    public static BoxButtonUI getBoxButtonUI() {
        return boxButtonUI;
    }

    public static void setBoxButtonUI(BoxButtonUI boxButtonUI) {
        UITask.boxButtonUI = boxButtonUI;
    }

    public static ProfilesPalletUI getProfilesPallet() {
        return profilesPallet;
    }

    public static void setProfilesPallet(ProfilesPalletUI profilesPallet) {
        UITask.profilesPallet = profilesPallet;
    }

    public static ObservableList<Profile> getProfiles() {
        return profiles;
    }

    public static void setProfiles(ObservableList<Profile> profiles) {
        UITask.profiles = profiles;
    }

    public static void populateProfilePallet() {
        //setting profile in the left panel...
        Platform.runLater(() -> {
            profilesPallet.getProfileList().setItems(profiles);
        });
    }

    public static Profile createProfile() {
        Profile profile = new Profile("profile: " + UITask.profiles.size());
        return profile;
    }

    public static void selectAddedProfile() {
        if (!Platform.isFxApplicationThread())
            throw new IllegalTaskCreationException("Operation Restricted: Not under UI thread!!!");
        Platform.runLater(() -> {
            profilesPallet.getProfileList().getSelectionModel().selectLast();
            try {
                Container.setCurrentTask(UITask.getSelectedTask());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }

    public static Profile getSelectedProfile() {
        return profilesPallet.getProfileList().getSelectionModel().getSelectedItem();
    }

    public static UITask getSelectedTask() throws Exception {
        return Container.getInstance().getUITask(getSelectedProfile());
    }

    public static void updateProperties() {
        UITask.setMinimized(true);
    }

    /*to make sure each recording is captured in right mode*/
    public static void toggleMode() {
        if (mode.equals("B"))
            mode = "F";
        if (mode.equals("F"))
            mode = "B";
    }

    /* initializing ui task processes */
    private void setProcessContext() {
        process = (event) -> {
            event.consume();
            String id = ((Button) event.getSource()).getId();
            System.out.println("inside button: " + id);
            Optional<TextField> textField = textFieldList.stream().filter(field -> field.getId().equals(id)).findFirst();
            if (textField.isPresent()) {
                System.out.println("inside the text field: " + id);
                AppUnit recorder = Hook.getInstance().activeHook();
                if (recorder instanceof Recorder) {
                    System.out.println("inside recorder: " + id);
                    textField.get().clear();
                    switch (((Button) event.getSource()).getText()) {
                        case BOX_BUTTON: {
                            System.out.println("inside BOX BUTTON: " + id);
                            try {
                                Platform.runLater(() -> {
                                    ((Recorder) recorder).getExternalRecordedEventSeq().stream().forEach(data -> textField.get().appendText(data.getKeyName()));
                                });
                                if (((Recorder) recorder).getStandardRecordedCount() > 0)
                                    EventBinder.bind(((Recorder) recorder).getExternalRecordedEventSeq(), ((Recorder) recorder).getStandardRecordedEventSeq(), profileUI);
                            } catch (NullPointerException e) {
                                //do nothing...
                            }
                            break;
                        }
                        case MAKE_BUTTON: {
                            System.out.println("inside MAKE BUTTON: " + id);
                            try {
                                Platform.runLater(() -> {
                                    ((Recorder) recorder).getStandardRecordedEventSeq().stream().forEach(data -> textField.get().appendText(data.getKeyName()));
                                });
                                if (((Recorder) recorder).getExternalRecordedCount() > 0)
                                    EventBinder.bind(((Recorder) recorder).getExternalRecordedEventSeq(), ((Recorder) recorder).getStandardRecordedEventSeq(), profileUI);
                            } catch (NullPointerException e) {
                                //do nothing...
                            }
                        }
                    }
                }
            }
        };
    }

    public ProfileUI getProfileUI() {
        return profileUI;
    }

    public void addBindingOption() {

        // Add the Button and text fields to the HBox so that to make binding recorded in the HBox...
        // this would add a box button followed by a key pressed from external game pad
        // and then again a box button followed by standard inputs events like keyboard,mouse, so on.

        addBinding();
        GridPane bindingPane = profileUI.getBindingPane();
        if (boxButtonUI.getBindings().getChildren().size() > 0)
            boxButtonUI.getBindings().getChildren().set(0, bindingPane);
        else
            boxButtonUI.getBindings().getChildren().add(0, bindingPane);
        System.out.println(bindingPane.getChildren().stream().count());
    }

    public void removeBindingOption() {

    }

    /**
     * routine to render UI components in javafx.scenes based on profile selected...
     */
    public void renderProfile() {
        GridPane bindingPane = null;
        /* checks whether binding pane is already there in the application context*/
        if (profileUI.getBindingPane().getChildren().size() > 0) {
            bindingPane = profileUI.getBindingPane();
        } else if (profileUI.getProfile().getBindings() != null && profileUI.getProfile().getBindings().size() > 0) {
            populateBindingPane(profileUI.getProfile().getBindings());
        } else {
            bindingPane = profileUI.getBindingPane();
        }
        if (boxButtonUI.getBindings().getChildren().size() > 0)
            boxButtonUI.getBindings().getChildren().set(0, bindingPane);
        else
            boxButtonUI.getBindings().getChildren().add(0, bindingPane);
        /* setting current process context for event handlers */
        this.setProcessContext();
    }

    private void populateBindingPane(Map<String, String> bindingKeys) {
        /* reset binding pane row index to 0... */
        profileUI.resetRowIndex();
        bindingKeys.entrySet().stream().forEach(entry -> {
            addBinding(entry.getKey(), entry.getValue());
        });
    }

    private void addBinding(String external, String standard) {
        String id = UUID.randomUUID().toString();
        TextField externalEvent = new TextField();
        externalEvent.setId(id);
        externalEvent.setText(external);
        externalEvent.setDisable(true);
        textFieldList.add(externalEvent);
        Button boxButtonB1 = new Button(BOX_BUTTON);
        boxButtonB1.setId(id);
        boxButtonB1.setOnAction(boxButtonHandler1);

        id = UUID.randomUUID().toString();
        TextField standardEvent = new TextField();
        standardEvent.setId(id);
        standardEvent.setText(standard);
        standardEvent.setDisable(true);
        textFieldList.add(standardEvent);
        Button boxButtonB2 = new Button(MAKE_BUTTON);
        boxButtonB2.setId(id);
        boxButtonB2.setOnAction(boxButtonHandler2);
        ComboBox presetBox = new ComboBox();
        presetBox.setItems(UITask.getOptionsAndPresetsList().optionsAndPresetsProperty().get());
        GridPane bindingPane = profileUI.getBindingPane();
        GridPane.setMargin(presetBox, new Insets(5, 200, 5, 100));
        bindingPane.addRow(profileUI.getRowIndex(), boxButtonB1, externalEvent, boxButtonB2, standardEvent, presetBox);
        bindingPane.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 4;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: blue;");
    }

    private void addBinding() {
        String id = UUID.randomUUID().toString();
        TextField externalEvent = new TextField();
        externalEvent.setId(id);
        externalEvent.setDisable(true);
        textFieldList.add(externalEvent);
        Button boxButtonB1 = new Button(BOX_BUTTON);
        boxButtonB1.setId(id);
        boxButtonB1.setOnAction(boxButtonHandler1);

        id = UUID.randomUUID().toString();
        TextField standardEvent = new TextField();
        standardEvent.setId(id);
        standardEvent.setDisable(true);
        textFieldList.add(standardEvent);
        Button boxButtonB2 = new Button(MAKE_BUTTON);
        boxButtonB2.setId(id);
        boxButtonB2.setOnAction(boxButtonHandler2);
        ComboBox presetBox = new ComboBox();
        presetBox.setItems(UITask.getOptionsAndPresetsList().optionsAndPresetsProperty().get());
        GridPane bindingPane = profileUI.getBindingPane();
        GridPane.setMargin(externalEvent, new Insets(5, 200, 5, 10));
        GridPane.setMargin(presetBox, new Insets(5, 200, 5, 100));
        bindingPane.addRow(profileUI.getRowIndex(), boxButtonB1, externalEvent, boxButtonB2, standardEvent, presetBox);
        bindingPane.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 4;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: blue;");
    }

    public void selectProfile() {
        Platform.runLater(() -> {
            profilesPallet.getProfileList().getSelectionModel().select(profileUI.getProfile());
        });
    }

    public void launch() {
        UITask.populateProfilePallet();
        /* it would select and render the profile contents...*/
        UITask.selectAddedProfile();

    }

    @FunctionalInterface
    public static interface Executor {
        void execute(ActionEvent event);
    }
}
