package com.box.controllers.ui;

import com.box.controllers.components.model.Profile;
import javafx.scene.layout.GridPane;

public class ProfileUI {

    private Profile profile;
    private GridPane bindingPane = new GridPane();
    private int rowIndex = 0;

    public void resetRowIndex() {
        this.rowIndex = 0;
    }

    public GridPane getBindingPane() {
        return bindingPane;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public int getRowIndex() {
        rowIndex++;
        return rowIndex - 1;
    }

}
