package com.box.controllers.ui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class NamedInstance {
    protected final StringProperty nameProperty = new SimpleStringProperty();

    public NamedInstance(String name) {
        this.nameProperty.set(name);
    }

    public StringProperty nameProperty() {
        return nameProperty;
    }

    public String getName() {
        return nameProperty.get();
    }

    @Override
    public String toString() {
        return getName();
    }
}