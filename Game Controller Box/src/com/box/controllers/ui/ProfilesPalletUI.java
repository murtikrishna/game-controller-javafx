package com.box.controllers.ui;

import com.box.controllers.components.model.Profile;
import javafx.scene.control.ListView;

public class ProfilesPalletUI {

    private ListView<Profile> profileList;

    public ProfilesPalletUI(ListView<Profile> profileList) {
        this.profileList = profileList;
    }

    public ListView<Profile> getProfileList() {
        return profileList;
    }
}
