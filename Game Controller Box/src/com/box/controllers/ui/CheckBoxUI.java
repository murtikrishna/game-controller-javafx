package com.box.controllers.ui;

import javafx.scene.control.CheckBox;

/**
 * holds references to CheckBox in main windows fxml...
 */
public class CheckBoxUI {


    private CheckBox mouseMoveRecording;

    private CheckBox mouseClickRecording;

    private CheckBox keyPressRecording;

    private CheckBox backgroundRecording;

    public CheckBoxUI(CheckBox mouseMoveRecording, CheckBox mouseClickRecording, CheckBox keyPressRecording, CheckBox backgroundRecording) {
        this.mouseMoveRecording = mouseMoveRecording;
        this.mouseClickRecording = mouseClickRecording;
        this.keyPressRecording = keyPressRecording;
        this.backgroundRecording = backgroundRecording;
    }

    public CheckBox getMouseMoveRecording() {
        return mouseMoveRecording;
    }

    public CheckBox getMouseClickRecording() {
        return mouseClickRecording;
    }

    public CheckBox getKeyPressRecording() {
        return keyPressRecording;
    }

    public CheckBox getBackgroundRecording() {
        return backgroundRecording;
    }
}
