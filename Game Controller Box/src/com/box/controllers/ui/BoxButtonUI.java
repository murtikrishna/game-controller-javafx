package com.box.controllers.ui;

import javafx.scene.layout.HBox;

/**
 * holds references to HBox in main windows fxml...
 */
public class BoxButtonUI {

    private HBox bindings;

    public BoxButtonUI(HBox bindings) {
        this.bindings = bindings;
    }

    public HBox getBindings() {
        return bindings;
    }
}
