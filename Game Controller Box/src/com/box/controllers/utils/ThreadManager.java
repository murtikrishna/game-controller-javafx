package com.box.controllers.utils;


import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.ui.UITask;
import javafx.event.ActionEvent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ThreadManager implements Cloneable {

    private static ThreadManager threadManager = null;

    private ExecutorService executor = Executors.newFixedThreadPool(5);

    private volatile boolean exit = false;

    private ThreadManager(ExecutorService executor) {
        this.executor = executor;
    }

    private ThreadManager() {

    }

    synchronized public static ThreadManager getInstance() throws Exception {
        if (threadManager == null)
            threadManager = new ThreadManager();
        return threadManager;
    }

    public boolean isExit() {
        return exit;
    }

    public void doExit(boolean exit) {
        this.exit = exit;
    }

    public void createTask(MessageEvent event, Consumer<MessageEvent> process) {

        if (process == null)
            return;

        Runnable task = new Runnable() {
            @Override
            public void run() {
                process.accept(event);
            }
        };
        executor.execute(task);
    }

    public void createUITask(UITask.Executor process, ActionEvent event) {

        Runnable task = new Runnable() {
            @Override
            public void run() {
                process.execute(event);
            }
        };
        executor.execute(task);
    }

    public void stopTaskCreation() {
        executor.shutdown();
    }

    public void done() {
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
    }

    @Override
    protected final Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("can not copy the instance!...shut down the running instance first");
    }
}
