package com.box.controllers.exception;

public class ComponentException extends RuntimeException {
    public ComponentException(String s) {
        super(s);
    }
}
