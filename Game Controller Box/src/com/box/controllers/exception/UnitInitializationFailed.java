package com.box.controllers.exception;

public class UnitInitializationFailed extends RuntimeException{
    public UnitInitializationFailed(String s) {
        super(s);
    }
}
