package com.box.controllers.exception;

public class RootComponentInitializationFailed extends RuntimeException {
    public RootComponentInitializationFailed(String s) {
        super(s);
    }
}
