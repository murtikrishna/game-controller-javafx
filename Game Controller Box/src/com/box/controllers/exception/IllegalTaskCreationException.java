package com.box.controllers.exception;

public class IllegalTaskCreationException extends RuntimeException {
    public IllegalTaskCreationException(String s) {
        super(s);
    }
}
