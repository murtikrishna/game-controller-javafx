package com.box.controllers.exception;

public class DataSourceException extends RuntimeException{

    public DataSourceException(String message) {
        super(message);
    }
}
