package com.box.controllers.components.eventbinder;

import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.ui.ProfileUI;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class EventBinder {

    private static Map<String, String> bind = null;

    public static void bind(List<MessageEvent> externalRecordedEventSeq, List<MessageEvent> standardRecordedEventSeq, ProfileUI profileUI) {
        profileUI.getProfile().getBindings().put(String.valueOf(externalRecordedEventSeq.get(0).getKeyCode()), standardRecordedEventSeq.stream().map(event -> event.getKeyCode() + ",").collect(Collectors.joining()));
        bind = profileUI.getProfile().getBindings();
    }

    public static Map<String, String> getSimulatorBindings() {
        return bind;
    }

    public static void main(String[] args) {
        String sg = "43,8,89,3,";
        String[] am = sg.split(",");
        Arrays.stream(am).forEach(System.out::println);
        System.out.println(Arrays.stream(am).map(e -> e + "o").collect(Collectors.joining()));
    }
}
