package com.box.controllers.components.model;

import com.box.controllers.ui.NamedInstance;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Option extends NamedInstance {

    private final ListProperty<Preset> PRESETS_PROPERTY;
    private String software;
    private String desc;
    private Set<String> macros = new HashSet<>();


    public Option(String name, ObservableList<Preset> presets) {
        super(name);
        PRESETS_PROPERTY = new SimpleListProperty<Preset>(presets);
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Set<String> getMacros() {
        return macros;
    }

    public void setMacros(Set<String> macros) {
        this.macros = macros;
    }

    public StringProperty nameProperty() {
        return super.nameProperty;
    }

    public ListProperty<Preset> presetsProperty() {
        return this.PRESETS_PROPERTY;
    }

    @Override
    public String toString() {
        return getName();
    }
}