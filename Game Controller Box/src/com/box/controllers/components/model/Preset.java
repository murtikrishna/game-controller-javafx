package com.box.controllers.components.model;

import com.box.controllers.ui.NamedInstance;
import javafx.beans.property.StringProperty;

public class Preset extends NamedInstance {

    public Preset(String name) {
        super(name);
    }

    public StringProperty nameProperty() {
        return super.nameProperty;
    }
}