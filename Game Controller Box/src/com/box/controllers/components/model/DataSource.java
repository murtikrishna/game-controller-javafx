/**
 * Datasource is the key entry point for data access through out entire application...
 * this class preserves singleton pattern...
 */
package com.box.controllers.components.model;

import com.box.controllers.exception.DataSourceException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DataSource implements Cloneable {

    private static DataSource instance = null;
    private List<Path> profilePaths = null;
    private Path presetsDirectory = null;
    private String home;
    private String extension;
    private ObservableList<Profile> profileObservableList;
    private ObservableList<Option> presetObservableList;

    private DataSource() throws Exception {
        try {
            profilePaths = new ArrayList<>();
            extension = ".profile";
            Properties properties = new Properties();
            properties.load(new FileInputStream(new File("settings.properties")));
            System.setProperties(properties);
            home = System.getProperty("profilesDirectory");
            presetsDirectory = Paths.get(System.getProperty("presetsDirectory"));
            home = (home == null) ? Paths.get("")
                    .toAbsolutePath()
                    .toString() : home;
            Files.newDirectoryStream(Paths.get(home),
                    path -> path.toString().endsWith(".profile"))
                    .forEach(path -> {
                        profilePaths.add(path);
                        System.out.println("found path for profile file: " + path);
                    });
            profileObservableList = FXCollections.observableArrayList();
            presetObservableList = FXCollections.observableArrayList();
            /* establishing data pipeline after all good*/
            System.out.println("loading profiles from directory...");
            loadProfiles();
            System.out.println("loaded profiles from directory...");
            System.out.println("loading presets from directory...");
            loadPresets();
        } catch (IOException e) {
            throw new Exception("Error while fetching profile paths in the directory: " + home + " ...Exception: " + e.getMessage());
        }
    }

    synchronized public static DataSource getInstance() throws Exception {
        if (instance == null)
            instance = new DataSource();
        return instance;
    }

    /* loads presets from excel sheet*/
    private void loadPresets() {

    }

    public ObservableList<Option> getPresetList() {
        return presetObservableList;
    }

    public ObservableList<Option> getPresetListHardCoded() {
        ObservableList<Option> options = FXCollections.observableArrayList();
        {
            //1st option
            Preset preset = new Preset("Ctrl+Up");
            ObservableList<Preset> presetList = FXCollections.observableArrayList();
            presetList.add(preset);
            preset = new Preset("Ctrl+Down");
            presetList.add(preset);
            Option option = new Option("OptiShot", presetList);
            options.add(option);
            //2nd option
            preset = new Preset("Alt+Shift+Up");
            presetList = FXCollections.observableArrayList();
            presetList.add(preset);
            preset = new Preset("Alt+Shift+Down");
            presetList.add(preset);
            option = new Option("Generic", presetList);
            options.add(option);
        }
        return options;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("can not be copied!");
    }

    public ObservableList<Profile> getProfileList() {
        return profileObservableList;
    }

    public void addProfile(Profile profile) {
        profileObservableList.add(profile);
    }

    /**
     * called during app initialization...
     *
     * @throws DataSourceException
     */
    public void loadProfiles() throws DataSourceException {
        for (Path path : profilePaths) {
            try (FileChannel channel = FileChannel.open(path)) {
                FileLock lock = null;
                try {
                    lock = channel.lock();
                } catch (IOException ioException) {
                    throw new DataSourceException("Error while acquiring lock to the data source file: " + ioException.getMessage());
                }
                try (ObjectInputStream in = new ObjectInputStream(Channels.newInputStream(channel))) {
                    try {
                        Profile profile = (Profile) in.readObject();
                        profileObservableList.add(profile);
                    } catch (ClassNotFoundException e) {
                        throw new DataSourceException("Wrong Profile format or incorrect serial version id of class used: " + e.getMessage());
                    } finally {
                        in.close();
                        lock.release();
                    }
                } finally {
                    lock.release();
                }
            } catch (IOException ioException) {
                throw new DataSourceException("Error while reading profile data using channels: " + ioException.getMessage());
            }
        }
    }

    /**
     * called before app is being closed...
     *
     * @throws DataSourceException
     */
    public void storeProfiles() throws DataSourceException {

        profileObservableList.stream().forEach(profile -> {
            try (FileChannel channel = FileChannel.open(Paths.get(home + FileSystems.getDefault().getSeparator() + profile.getName() + extension))) {
                FileLock lock = null;
                try {
                    lock = channel.lock();
                } catch (IOException ioException) {
                    throw new DataSourceException("Error while acquiring lock to the data source file: " + ioException.getMessage());
                }
                try (ObjectOutputStream out = new ObjectOutputStream(Channels.newOutputStream(channel))) {
                    out.writeObject(profile);
                } finally {
                    lock.release();
                }
            } catch (IOException ioException) {
                throw new DataSourceException("Error while writing profile data using channels: " + ioException.getMessage());
            }
        });
    }
}
