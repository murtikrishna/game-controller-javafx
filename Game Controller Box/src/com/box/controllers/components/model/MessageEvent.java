package com.box.controllers.components.model;


import com.box.controllers.exception.UnsupportedFormatForEventException;

public class MessageEvent {

    /* 'standard' indicates system events like mouse , click or keyboard
     * allowed values are 'standard', , 'external'
     * */
    private String occurred = "standard";
    private Enum type;
    private int keyCode;
    private String keyName;

    public MessageEvent() {
        type = Type.None;
        keyCode = -1;
        keyName = "";
    }

    public MessageEvent(int keyCode, String keyName) {
        this.keyCode = keyCode;
        this.keyName = keyName;
    }

    /**
     * @param occurred allowed values are 'standard', 'external'
     * @param type
     * @param keyCode
     * @param keyName
     */
    public MessageEvent(String occurred, Enum type, int keyCode, String keyName) {
        if (occurred.equals("standard") || occurred.equals("external"))
            this.occurred = occurred;
        else throw new UnsupportedFormatForEventException("event source type only allowed as 'standard' or 'external'");
        this.type = type;
        this.keyCode = keyCode;
        this.keyName = keyName;
    }

    public String getSource() {
        return occurred;
    }

    public String getOccurred() {
        return occurred;
    }

    public Enum getType() {
        return type;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public String getKeyName() {
        return keyName;
    }

    enum Type {
        None, CommandKey, Key, SpecialKey, Mouse, Click
    }
}
