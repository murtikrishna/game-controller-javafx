package com.box.controllers.components.model;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.ObservableList;

public class Presets {
    private static final ListProperty<Option> OPTIONS_PROPERTY = new SimpleListProperty<Option>();

    public static void addOptions(ObservableList<Option> options) {
        OPTIONS_PROPERTY.set(options);
    }

    public static ListProperty<Option> optionsProperty() {
        return OPTIONS_PROPERTY;
    }
}