package com.box.controllers.components.main;

import com.box.controllers.components.commlayers.Hook;
import com.box.controllers.components.commlayers.StandardInputService;
import com.javacodegeeks.snippets.desktop.HelloWorld;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;


import javax.swing.*;

public class Main extends Application {

    private static void initializeToolkits() {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        JFrame frame = new JFrame("Swing Defaulter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add the ubiquitous "Hello World" label.
        JLabel label = new JLabel("ToolKit Initializations");
        frame.getContentPane().add(label);

        //load the UI and stop rendering to the screen.
        frame.pack();
        frame.setVisible(false);
    }

    public static void main(String[] args) {

        /* launch swing to set default awt toolkits*/
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                initializeToolkits();
            }
        });
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("mainWindow.fxml"));
        primaryStage.setTitle("Controller Box - SIMULATOR");
        Rectangle2D r = Screen.getPrimary().getBounds();
        Scene scene = new Scene(root, r.getWidth(), r.getHeight());
        primaryStage.setScene(scene);
        /* initializing and building container and app components */
        Container container = Container.getInstance();
        container.register(primaryStage);
        container.initialize();
        System.out.println("successfully built container");
        container.getComponents().entrySet().stream().forEach(e -> System.out.println(e.getValue().toString()));
        System.out.println("Active Hook: " + Hook.getInstance().activeHook().toString());
        primaryStage.show();
    }

}
