package com.box.controllers.components.main;

import com.box.controllers.components.model.Profile;
import com.box.controllers.ui.BoxButtonUI;
import com.box.controllers.ui.CheckBoxUI;
import com.box.controllers.ui.ProfilesPalletUI;
import com.box.controllers.ui.UITask;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

public class MainController {

    @FXML
    private ListView<Profile> profileList;

    @FXML
    private CheckBox mouseMoveRecording;
    @FXML
    private CheckBox mouseClickRecording;
    @FXML
    private CheckBox keyPressRecording;
    @FXML
    private CheckBox backgroundRecording;

    @FXML
    private HBox bindings;

    @FXML
    public void deleteProfile(ActionEvent actionEvent) {
    }

    @FXML
    public void addBinding(ActionEvent actionEvent) throws Exception {
        UITask task = Container.getCurrentTask();
        task.addBindingOption();
    }

    @FXML
    public void deleteBinding(ActionEvent actionEvent) {

    }

    public void addProfile(ActionEvent actionEvent) {
        Profile profile = UITask.createProfile();
        UITask task = new UITask(profile);
        task.launch();
        Container.getInstance().getUiComponents().add(Container.getInstance().register(task));
    }

    public void renameProfile(ActionEvent actionEvent) {
        Profile profile = UITask.getSelectedProfile();
        profile.setName("");
    }

    public void initialize() {
        System.out.println("injecting windows profile pallet and binding panes into app UITask's bucket...");
        UITask.setProfilesPallet(new ProfilesPalletUI(profileList));
        UITask.getProfilesPallet().getProfileList().setEditable(true);
        UITask.setBoxButtonUI(new BoxButtonUI(bindings));
        UITask.setCheckBoxUI(new CheckBoxUI(mouseMoveRecording, mouseClickRecording, keyPressRecording, backgroundRecording));
    }

}
