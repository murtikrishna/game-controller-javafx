package com.box.controllers.components.main;

import com.box.controllers.components.commlayers.AppUnit;
import com.box.controllers.components.commlayers.Hook;
import com.box.controllers.components.model.DataSource;
import com.box.controllers.components.model.Presets;
import com.box.controllers.components.model.Profile;
import com.box.controllers.components.unit.recorder.Recorder;
import com.box.controllers.components.unit.simulators.ControllerRobot;
import com.box.controllers.exception.UnitInitializationFailed;
import com.box.controllers.ui.OptionsAndPresetsList;
import com.box.controllers.ui.UITask;
import com.box.controllers.utils.ThreadManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.*;

/**
 * registers all the units and background processes and keep tracks...
 */
public class Container {
    static Container container = null;
    private static List<String> units;
    private static List<String> uiComponents;
    private static UITask currentTask = null;
    private Map<String, Object> bucket = new LinkedHashMap();
    private Stage stage = null;

    private Container() {
        units = new ArrayList<>();
        uiComponents = new ArrayList<>();
    }

    public static UITask getCurrentTask() {
        return currentTask;
    }

    public static void setCurrentTask(UITask uiTask) {
        currentTask = uiTask;
    }

    public static List<String> getUiComponents() {
        return uiComponents;
    }

    synchronized public static Container getInstance() {
        if (container == null)
            container = new Container();
        return container;
    }


    public Stage getStage() {
        return stage;
    }

    synchronized public void initialize() throws Exception {
        //initializing units and its dependent components...
        initializeUnits();
        initializeUIComponents();
        //initializing thread managers...
        register(ThreadManager.getInstance());
    }

    private void initializeUIComponents() throws Exception {

        /* initializing data stream and datasource*/
        UITask.setProfiles(DataSource.getInstance().getProfileList());
        System.out.println("initializing UI options and priest list observable list...");
        UITask.setOptionsAndPresetsList(new OptionsAndPresetsList(Presets.optionsProperty()));
        if (DataSource.getInstance().getPresetList().size() > 0)
            Presets.addOptions(DataSource.getInstance().getPresetList());
        else
            Presets.addOptions(DataSource.getInstance().getPresetListHardCoded());
        System.out.println("Successfully established data source pipelines...");
        /* data initialization ends here*/

        UITask.getProfiles().stream().forEach((profile) -> {
            UITask task = new UITask(profile);
            uiComponents.add(Container.getInstance().register(task));
        });
        UITask.populateProfilePallet();
        if (currentTask != null) {
            currentTask.selectProfile();
            currentTask.renderProfile();
        }

        /* initializing listeners...*/
        System.out.println("initializing UI listeners from within container...");
        UITask.getProfilesPallet().getProfileList().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Profile>() {
            @Override
            public void changed(ObservableValue<? extends Profile> observable, Profile oldValue, Profile newValue) {
                if (newValue != null) {
                    try {
                        currentTask = UITask.getSelectedTask();
                        currentTask.renderProfile();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        });
        UITask.getProfilesPallet().getProfileList().setCellFactory(param -> {
            TextFieldListCell<Profile> cell = new TextFieldListCell<Profile>(new StringConverter<Profile>() {
                @Override
                public String toString(Profile object) {
                    return object.getName();
                }

                @Override
                public Profile fromString(String string) {
                    UITask.getSelectedProfile().setName(string);
                    return UITask.getSelectedProfile();
                }
            });
            return cell;
        });
        stage.iconifiedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                System.out.println("minimized:" + t1.booleanValue());
                if (UITask.isBackgroundEnabled()) {
                    if (t1) {
                        if (UITask.isMouseMoveEnabled())
                            Recorder.getInstance();
                        if (UITask.isMouseClickEnabled())
                            Recorder.getInstance();
                        if (UITask.isKeyPressedEnabled())
                            Recorder.getInstance();
                        UITask.updateProperties();
                    } else {
                        UITask.setMinimized(false);
                    }
                    try {
                        startUnit(Recorder.getInstance().toggleMode());
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        });
        stage.maximizedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                System.out.println("maximized:" + t1.booleanValue());
            }
        });
    }

    public UITask getUITask(Profile profile) throws Exception {
        try {
            UITask task = (UITask) bucket.entrySet().stream().filter((e) -> {
                return uiComponents.contains(e.getKey()) && ((UITask) e.getValue()).getProfileUI().getProfile().equals(profile);
            }).findFirst().get().getValue();
            return task;
        } catch (Exception e) {
            throw new Exception("UI Task component not found..." + e.getMessage());
        }
    }

    private void initializeUnits() {
        try {
            /* default initialization to recorder*/
            Recorder recorder = Recorder.getInstance();
            units.add(Container.getInstance().register(recorder));
            ControllerRobot robot = ControllerRobot.getInstance();
            units.add(Container.getInstance().register(robot));
            startDefault();
        } catch (Exception e) {
            throw new UnitInitializationFailed("Failed while initializing units: " + e.getMessage());
        }
    }

    private void startDefault() {
        //starts recorder by default...
        Hook.getInstance().switchHook(Recorder.getInstance());
    }

    private void startUnit(AppUnit appUnit) {
        //starts recorder by default...
        Hook.getInstance().switchHook(appUnit);
    }

    public String register(Object component) {

        boolean isComponentExists = bucket.entrySet().stream().anyMatch((entry) -> {
            return entry.getValue() == component;
        });
        if (!isComponentExists) {

            String id = UUID.randomUUID().toString();

            if (component.getClass() == Stage.class)
                stage = (Stage) component;

            bucket.put(id, component);
            return id;
        }

        return (String) bucket.entrySet().stream().filter(
                (entry) -> {
                    return entry.getValue() == component;
                }).map(entry -> entry.getKey()).findFirst().get();
    }

    public Map<String, Object> getComponents() {
        return bucket;
    }

    public Object getComponent(String id) {
        return bucket.get(id);
    }

}
