package com.box.controllers.components.unit.windows;

import com.box.controllers.components.commlayers.AppUnit;
import com.box.controllers.components.commlayers.JAVAFXInputService;
import com.box.controllers.components.commlayers.ListenerService;
import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.AppUnitListenerException;
import com.box.controllers.exception.ComponentException;
import com.box.controllers.utils.ThreadManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

public class WindowRecorder extends AppUnit {

    private static WindowRecorder windowRecorder = null;
    private static Properties recordingProperties = new Properties();
    private List<MessageEvent> standardRecordedEventSeq = null;
    private List<MessageEvent> externalRecordedEventSeq = null;
    private boolean clearFirst = false;

    public WindowRecorder() {
        super(new ArrayList<>());
        init();
        initializeServices();
    }

    synchronized public static WindowRecorder getInstance() {
        if (windowRecorder == null)
            windowRecorder = new WindowRecorder();
        return windowRecorder;
    }

    public List<MessageEvent> getStandardRecordedEventSeq() {
        List<MessageEvent> temp = standardRecordedEventSeq;
        standardRecordedEventSeq = new ArrayList<>();
        return temp;
    }

    public List<MessageEvent> getExternalRecordedEventSeq() {
        List<MessageEvent> temp = externalRecordedEventSeq;
        externalRecordedEventSeq = new ArrayList<>();
        return temp;
    }

    private void initializeServices() {
        super.services.add(Container.getInstance().register(JAVAFXInputService.getInstance()));
    }

    public int getExternalRecordedCount() {
        return externalRecordedEventSeq.size();
    }

    public int getStandardRecordedCount() {
        return standardRecordedEventSeq.size();
    }

    @Override
    public void listen(MessageEvent messageEvent) {
        try {
            Consumer<MessageEvent> process = null;
            if (messageEvent.getSource().equals("standard")) {
                process = event -> {
                    try {
                        standardRecordedEventSeq.add(messageEvent);
                    } catch (NullPointerException e) {
                        throw new AppUnitListenerException("Unable to listen the event properly....services or data lack(recorder data not initialized): " + e.getMessage());
                    }
                };
            } else if (messageEvent.getSource().equals("external")) {
                process = event -> {
                    try {
                        externalRecordedEventSeq.add(messageEvent);
                    } catch (NullPointerException e) {
                        throw new AppUnitListenerException("Unable to listen the event properly....services or data lack(recorder data not initialized): " + e.getMessage());
                    }
                };
            }
            ThreadManager.getInstance().createTask(messageEvent, process);

        } catch (Exception exception) {
            throw new AppUnitListenerException("Unable to process--- thread creation error: " + exception.getMessage());
        }
    }

    private void init() {
        standardRecordedEventSeq = new ArrayList<>();
        externalRecordedEventSeq = new ArrayList<>();
        recordingProperties.setProperty("mouseClick", String.valueOf(true));
        recordingProperties.setProperty("mouseMovement", String.valueOf(true));
        recordingProperties.setProperty("KeyPressed", String.valueOf(true));
        recordingProperties.setProperty("backgroundEnabled", String.valueOf(true));
    }

    @Override
    public void start() {
        System.out.println("Starting Application;s Window Recorder...");
        super.start();
    }

    @Override
    public void stop() {
        System.out.println("Stopping Application's Window Recorder...");
        super.stop();
    }

    @Override
    public String toString() {
        return "WindowRecorder{" +
                "services=" + services +
                '}';
    }

}
