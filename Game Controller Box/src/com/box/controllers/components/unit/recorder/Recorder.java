package com.box.controllers.components.unit.recorder;

import com.box.controllers.components.commlayers.*;
import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.AppUnitListenerException;
import com.box.controllers.exception.RootComponentInitializationFailed;
import com.box.controllers.utils.ThreadManager;

import java.util.*;
import java.util.function.Consumer;

/**
 * this class is a mutually exclusive active component dedicated for recording input events...
 */
public class Recorder extends AppUnit {
    private static Recorder recorder = null;
    private static Properties recordingProperties = new Properties();
    private List<MessageEvent> standardRecordedEventSeq = null;
    private List<MessageEvent> externalRecordedEventSeq = null;
    private boolean isBackgroundMode = false;

    private Recorder() {
        super(new ArrayList<>());
        init();
        initializeServices();
    }

    synchronized public static Recorder getInstance() {
        if (recorder == null)
            recorder = new Recorder();
        return recorder;
    }

    public Recorder toggleMode() throws Exception {

        if (isBackgroundMode == true) {
            String service = services.stream().filter((id) -> {
                boolean isTure = Container.getInstance().getComponent(id).getClass() == StandardInputService.class;
                if (isTure) {
                    try {
                        ((StandardInputService) Container.getInstance().getComponent(id)).stop();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                return isTure;
            }).findFirst().get();
            services.remove(service);
            JAVAFXInputService javafxInputService = JAVAFXInputService.getInstance();
            services.add(Container.getInstance().register(javafxInputService));
            javafxInputService.start();
            isBackgroundMode = false;
        } else {
            services.stream().filter(s -> {
                return true;
            }).forEach(e -> {
                System.out.println("id: " + e);
            });

            String service = services.stream().filter((id) -> {
                boolean isTure = Container.getInstance().getComponent(id).getClass() == JAVAFXInputService.class;
                if (isTure) {
                    try {
                        ((JAVAFXInputService) Container.getInstance().getComponent(id)).stop();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                return isTure;
            }).findFirst().get();
            services.remove(service);
            StandardInputService standardInputService = StandardInputService.getInstance();
            services.add(Container.getInstance().register(standardInputService));
            standardInputService.start();
            isBackgroundMode = true;
        }
        return this;
    }

    public List<MessageEvent> getStandardRecordedEventSeq() {
        List<MessageEvent> temp = standardRecordedEventSeq;
        standardRecordedEventSeq = new ArrayList<>();
        return temp;
    }

    public List<MessageEvent> getExternalRecordedEventSeq() {
        List<MessageEvent> temp = externalRecordedEventSeq;
        externalRecordedEventSeq = new ArrayList<>();
        return temp;
    }

    public int getExternalRecordedCount() {
        return externalRecordedEventSeq.size();
    }

    public int getStandardRecordedCount() {
        return standardRecordedEventSeq.size();
    }

    private void initializeServices() {

        try {
            //scanning external game controller to ensure is working and up...
            GameControllerService controller = GameControllerService.getInstance();
            //ensure the system keyboard and mouse listeners are up...
            JAVAFXInputService javafxInputService = JAVAFXInputService.getInstance();
            //registering components...
            services.add(Container.getInstance().register(javafxInputService));
            services.add(Container.getInstance().register(controller));
            isBackgroundMode = false;
        } catch (Exception e) {
            throw new RootComponentInitializationFailed("Recorder Initialization Failed: Failed while initializing controllers using base component architecture " + e.getMessage());
        }
    }

    private void init() {
        standardRecordedEventSeq = new ArrayList<>();
        externalRecordedEventSeq = new ArrayList<>();
        recordingProperties.setProperty("mouseClick", String.valueOf(true));
        recordingProperties.setProperty("mouseMovement", String.valueOf(true));
        recordingProperties.setProperty("KeyPressed", String.valueOf(true));
        recordingProperties.setProperty("backgroundEnabled", String.valueOf(true));
    }

    @Override
    public void listen(MessageEvent messageEvent) {
        try {
            Consumer<MessageEvent> process = null;
            if (messageEvent.getSource().equals("standard")) {
                process = event -> {
                    try {
                        standardRecordedEventSeq.add(messageEvent);
                    } catch (NullPointerException e) {
                        throw new AppUnitListenerException("Unable to listen the event properly....services or data lack(recorder data not initialized): " + e.getMessage());
                    }
                };
            } else if (messageEvent.getSource().equals("external")) {
                process = event -> {
                    try {
                        externalRecordedEventSeq.add(messageEvent);
                    } catch (NullPointerException e) {
                        throw new AppUnitListenerException("Unable to listen the event properly....services or data lack(recorder data not initialized): " + e.getMessage());
                    }
                };
            }
            ThreadManager.getInstance().createTask(messageEvent, process);

        } catch (Exception exception) {
            throw new AppUnitListenerException("Unable to process--- thread creation error: " + exception.getMessage());
        }
    }

    @Override
    public void start() {
        System.out.println("Starting Background Recorder...");
        super.start();
    }

    @Override
    public void stop() {
        System.out.println("Stopping Background Recorder...");
        super.stop();
    }

    @Override
    public String toString() {
        return "Recorder{" +
                "services=" + services +
                '}';
    }
}
