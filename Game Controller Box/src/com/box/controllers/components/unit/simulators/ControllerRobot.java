package com.box.controllers.components.unit.simulators;

import com.box.controllers.components.commlayers.GameControllerService;
import com.box.controllers.components.commlayers.AppUnit;
import com.box.controllers.components.commlayers.ListenerService;
import com.box.controllers.components.commlayers.StandardInputService;
import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.ComponentException;
import com.box.controllers.exception.RootComponentInitializationFailed;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.robot.Robot;
import javafx.stage.Stage;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import static javafx.application.Application.launch;

/**
 * this class is a mutually exclusive active component dedicated for simulating robots from captured game controller events ...
 * <p>
 * 1. a default implemented robot component to simulate all types of keys events from controller ports....
 * 2. basic task is to simulate one to one key pressed from game controllers
 * with the BIOS standard keys, mouse, movements and clicks...
 * 3. the robot can be activated and freezes as per the design toggle pattern in the underlying class (see toggleRobot)...
 * 4. implements the listener for any key pressed events arises...
 */
public class ControllerRobot extends AppUnit {

    private static ControllerRobot controllerRobot = null;
    private List<String> services = null;

    private ControllerRobot() {
        super(new ArrayList<>());
        initializeServices();
    }

    synchronized public static ControllerRobot getInstance() {
        if (controllerRobot == null)
            controllerRobot = new ControllerRobot();
        return controllerRobot;
    }

    public static void main(String[] args) {
        ControllerRobot.Test1.launch(ControllerRobot.Test1.class);
    }

    private static void simulate() {

        Platform.runLater(() -> {
            // These coordinates are screen coordinates

            int xCoord = 50;

            int yCoord = 100;

            Robot robot = new Robot();

            // Move the cursor

            robot.mouseMove(xCoord, yCoord);

            // Simulate a mouse click

            robot.mousePress();

            robot.mouseRelease();

            // Simulate a key press

            robot.keyPress(KeyCode.E);
        });
    }

    private void initializeServices() {
        super.services = new ArrayList<>();
        try {
            //scanning external game controller to ensure is working and up...
            GameControllerService controller = GameControllerService.getInstance();
            super.services.add(Container.getInstance().register(controller));
            //registering standard input services...
            StandardInputService standardInputService = StandardInputService.getInstance();
            super.services.add(Container.getInstance().register(standardInputService));
        } catch (Exception e) {
            throw new RootComponentInitializationFailed("Robot Initialization Failed: Failed while initializing controllers and robots: " + e.getMessage());
        }
    }

    @Override
    public void listen(MessageEvent messageEvent) {
        simulate();
    }

    @Override
    public void start() {
        System.out.println("Starting ControllerRobot to simulate the game pad events...");
        super.start();
    }

    @Override
    public void stop() {
        System.out.println("Stopping ControllerRobot...");
        super.stop();
    }

    @Override
    public String toString() {
        return "ControllerRobot{" +
                "services=" + services +
                '}';
    }

    class Test1 extends Application {

        @Override
        public void start(Stage primaryStage) throws Exception {
            simulate();
        }
    }
}
