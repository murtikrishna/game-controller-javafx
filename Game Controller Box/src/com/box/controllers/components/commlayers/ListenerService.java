package com.box.controllers.components.commlayers;

public abstract class ListenerService {

    public ListenerService() {

    }

    public void start() throws Exception {

    }

    public void stop() throws Exception {
    }

    abstract public boolean isStarted();
}
