package com.box.controllers.components.commlayers;

import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.RootComponentInitializationFailed;
import net.java.games.input.*;

/**
 * game controller listener...
 */
public class GameControllerService extends ListenerService {

    private static GameControllerService gameControllerService = null;
    private Controller controller = null;

    private boolean isEnabled = false;

    private GameControllerService(Controller controller) {
        this.controller = controller;
    }

    public static void main(String[] args) {
        GameControllerService.getInstance().start();
    }

    private static GameControllerService scanControllers() {

        /* Create an event object for the underlying plugin to populate */
        Event event = new Event();

        /* Get the available controllers */
        Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
        if (controllers.length == 0) {
            System.out.println("Found no controllers.");
            System.exit(0);
        }

        for (int i = 0; i < controllers.length; i++) {

            Controller controller = controllers[i];
            if (controller.getType() == Controller.Type.STICK || controller.getType() == Controller.Type.GAMEPAD
                    || controller.getType() == Controller.Type.WHEEL
                    || controller.getType() == Controller.Type.FINGERSTICK) {

                System.out.println("Found Game Pad Controller");
                /* initialize GameControllerComponent */
                if (gameControllerService == null)
                    gameControllerService = new GameControllerService(controller);
                return gameControllerService;
            }
            if (gameControllerService == null)
                gameControllerService = new GameControllerService(controller);
            return gameControllerService;
        }
        System.out.println("Finished scanning successfully...");
        System.out.println("Could not found game Controller...");
        throw new RootComponentInitializationFailed("Could not found game Controller...finishing task...");
    }

    public static GameControllerService getInstance() {
        //make sure controllers plugged in before getting instance and return controller
        return scanControllers();
    }

    @Override
    public void start() {
        isEnabled = true;
        startEventReceiverThread();
    }

    @Override
    public void stop() {
        if (!isEnabled)
            return;
        isEnabled = false;
    }

    public boolean isStarted() {
        return isEnabled;
    }

    private void startEventReceiverThread() {
        Thread eventReceiver = new Thread(() -> {
            readAllEvents();
        });
        eventReceiver.start();
    }

    private void readAllEvents() {
        while (isEnabled) {

            // Pull controller for current data, and break while loop if controller is
            // disconnected.
            if (!controller.poll()) {
                break;
            }

            // X axis and Y axis
            int xAxisPercentage = 0;
            int yAxisPercentage = 0;
            // JPanel for other axes.
            // JPanel for controller buttons

            // Go trough all components of the controller.
            Component[] components = controller.getComponents();
            for (int i = 0; i < components.length; i++) {
                Component component = components[i];
                Component.Identifier componentIdentifier = component.getIdentifier();

                // Buttons
                // if(component.getName().contains("Button")){ // If the language is not
                // english, this won't work.
                if (componentIdentifier.getName().matches("^[0-9]*$")) { // If the component identifier name contains
                    // only numbers, then this is a button.
                    // Is button pressed?
                    boolean isItPressed = true;
                    if (component.getPollData() == 0.0f)
                        isItPressed = false;

                    // We know that this component was button so we can skip to next component.
                    continue;
                }

                // Hat switch
                if (componentIdentifier == Component.Identifier.Axis.POV) {
                    float hatSwitchPosition = component.getPollData();
                    // We know that this component was hat switch so we can skip to next component.
                    continue;
                }

                // Axes
                if (component.isAnalog()) {
                    float axisValue = component.getPollData();
                    // X axis
                    if (componentIdentifier == Component.Identifier.Axis.X) {

                        continue; // Go to next component.
                    }
                    // Y axis
                    if (componentIdentifier == Component.Identifier.Axis.Y) {

                        continue; // Go to next component.
                    }

                    // Other axis

                }
            }

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "GameControllerService{" +
                "isEnabled=" + isEnabled +
                '}';
    }
}
