package com.box.controllers.components.commlayers;

import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.ComponentException;

import java.util.List;


/**
 * listen to any types of events come across...
 * reusable to start and stop...
 */
public abstract class AppUnit {

    protected List<String> services;

    public AppUnit(List<String> services) {
        this.services = services;
    }

    abstract public void listen(MessageEvent messageEvent);

    public void start() {
        services.stream().forEach(service -> {
            try {
                ((ListenerService) Container.getInstance().getComponent(service)).start();
                System.out.println("Started Successfully... with services id: " + service + ((ListenerService) Container.getInstance().getComponent(service)).isStarted());
            } catch (Exception exception) {
                throw new ComponentException("Exception while starting the Unit: " + exception.getMessage());
            }
        });
    }

    public void stop() {
        services.stream().forEach(service -> {
            try {
                ((ListenerService) Container.getInstance().getComponent(service)).stop();
                System.out.println("Stopped Successfully... with services id: " + service + ((ListenerService) Container.getInstance().getComponent(service)).isStarted());
            } catch (Exception exception) {
                throw new ComponentException("Exception while stopping the Unit: " + exception.getMessage());
            }
        });
    }
}
