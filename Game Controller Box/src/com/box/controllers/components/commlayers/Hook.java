package com.box.controllers.components.commlayers;

import com.box.controllers.components.model.MessageEvent;
import com.box.controllers.exception.AppUnitHookDispatchException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * adds implemented listener components like unit components or main components using extension-hook design...
 * enable or disable hooks at runtime as per requirements...
 */
public class Hook {

    private static Hook hook = new Hook();
    private Map<AppUnit, Boolean> hooks = new HashMap<AppUnit, Boolean>();

    public static Hook getInstance() {
        return hook;
    }

    public AppUnit switchHook(AppUnit appUnit) {
        toggle(appUnit);
        return appUnit;
    }

    public AppUnit activeHook() {
        Optional<Map.Entry<AppUnit, Boolean>> appUnit = hooks.entrySet().stream().findAny();
        return appUnit.isPresent() ? appUnit.get().getKey() : null;
    }

    private void toggle(AppUnit appUnit) {
        hooks.entrySet().stream().filter(e -> e.getValue() == true).forEach(e -> e.getKey().stop());
        hooks.entrySet().stream().filter(e -> e.getValue() == true).forEach(e -> e.setValue(false));
        hooks.put(appUnit, true);
        appUnit.start();
    }

    public void dispatch(MessageEvent messageEvent) {
        try {
            hooks.entrySet().stream().filter(e -> e.getValue() == true).forEach(e -> e.getKey().listen(messageEvent));
        } catch (Exception e) {
            throw new AppUnitHookDispatchException("Unable to dispatch or forward request properly to the app unit: " + e.getMessage());
        }
    }
}

