package com.box.controllers.components.commlayers;

import com.box.controllers.components.model.MessageEvent;
import javafx.application.Platform;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * listen to system standard events like keyboard and mouse...
 */
public class StandardInputService extends ListenerService implements NativeKeyListener, NativeMouseListener, NativeMouseMotionListener {

    private static StandardInputService standardInputService = null;

    static {
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(false);
    }

    private boolean keyReleased = false;
    private boolean isEnabled = false;
    private String name = "standard inputs service";

    private StandardInputService() {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.exit(1);
        }
    }

    public static StandardInputService getInstance() {
        return initialize();
    }

    public static void main(String[] args) {
        initialize();
        GlobalScreen.addNativeKeyListener(standardInputService);
        GlobalScreen.addNativeMouseListener(standardInputService);
        GlobalScreen.addNativeMouseMotionListener(standardInputService);
    }

    synchronized private static StandardInputService initialize() {
        if (standardInputService == null)
            standardInputService = new StandardInputService();
        return standardInputService;
    }

    public boolean isStarted() {
        return isEnabled;
    }

    public void start() throws Exception {
        try {
            GlobalScreen.unregisterNativeHook();
        } catch (NativeHookException ex) {
            System.exit(1);
        }
        /* restarts the hook*/
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.exit(1);
        }
        GlobalScreen.addNativeKeyListener(standardInputService);
        GlobalScreen.addNativeMouseListener(standardInputService);
        GlobalScreen.addNativeMouseMotionListener(standardInputService);
        isEnabled = true;
    }

    public void stop() throws Exception {
        if (!isEnabled)
            return;
        GlobalScreen.removeNativeKeyListener(standardInputService);
        GlobalScreen.removeNativeMouseListener(standardInputService);
        GlobalScreen.removeNativeMouseMotionListener(standardInputService);
        try {
            GlobalScreen.unregisterNativeHook();
        } catch (NativeHookException ex) {
            System.exit(1);
        }
        isEnabled = false;
    }


    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {
        String keyName = "not specified";

        keyName = NativeKeyEvent.getKeyText(e.getKeyCode());

        System.out.println("Key Pressed inside StandardInputHandler: " + keyName);

        if (e.getKeyCode() == NativeKeyEvent.ALT_R_MASK) {
            try {
                stop();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            Hook.getInstance().dispatch(new MessageEvent(e.getKeyCode(), keyName));
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        keyReleased = true;
        String keyName = "not specified";
        keyName = NativeKeyEvent.getKeyText(e.getKeyCode());
        System.out.println("key released: " + keyName);
        int modifiers = e.getModifiers();
        System.out.println("in key released modifiers: " + modifiers);
        // Hook.getInstance().dispatch(new Event(e.getKeyCode(), NativeKeyEvent.getKeyText(e.getKeyCode())));
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        System.out.println("in key typed");
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {

        System.out.println("mouse clicked");

    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseMoved(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {

    }

    @Override
    public String toString() {
        return "StandardInputService{" +
                "name='" + name + '\'' +
                '}';
    }
}