package com.box.controllers.components.commlayers;

import com.box.controllers.components.main.Container;
import com.box.controllers.components.model.MessageEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class JAVAFXInputService extends ListenerService {

    private static JAVAFXInputService javafxInputService = null;
    private boolean isEnabled = false;

    private JAVAFXInputService() {
        super();
    }

    synchronized public static JAVAFXInputService getInstance() {
        return initialize();
    }

    synchronized private static JAVAFXInputService initialize() {
        if (javafxInputService == null)
            javafxInputService = new JAVAFXInputService();
        return javafxInputService;
    }

    @Override
    public void start() throws Exception {
        Container.getInstance().getStage().getScene().setOnKeyPressed(this::handleKeyboardInput);
        Container.getInstance().getStage().getScene().setOnMouseMoved(this::handleMouseMove);
        Container.getInstance().getStage().getScene().setOnMouseClicked(this::handleMouseClicked);
        isEnabled = true;
    }

    @Override
    public void stop() throws Exception {
        if (!isEnabled)
            return;
        isEnabled = false;
    }

    private void handleMouseClicked(MouseEvent mouseEvent) {
        // System.out.println("entered into window's key pressed code: " + mouseEvent.getClickCount());
    }

    private void handleMouseMove(MouseEvent mouseEvent) {
        // System.out.println("entered into window's key pressed code: " + mouseEvent.getSceneX());
    }

    @Override
    public boolean isStarted() {
        return false;
    }

    @FXML
    public void handleKeyboardInput(KeyEvent keyEvent) {
        System.out.println("entered into window's key pressed code: " + keyEvent.getCode().getCode() + " text : " + keyEvent.getCode().getName());
        Hook.getInstance().dispatch(new MessageEvent(keyEvent.getCode().getCode(), keyEvent.getCode().getName()));
    }

}
