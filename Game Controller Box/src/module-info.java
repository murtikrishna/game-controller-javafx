module Game.Controller.Box {

    requires javafx.fxml;
    requires javafx.controls;
    requires java.desktop;
    requires jnativehook;
    requires java.logging;
    requires jinput;

    opens com.box.controllers.components.main;

    exports com.box.controllers.components.main;
}